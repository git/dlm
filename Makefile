RELEASE=4.0

# sources from: https://git.fedorahosted.org/git/dlm.git

DLMVER=4.0.2
PKGREL=1
DLMDIR=dlm-${DLMVER}
DLMSRC=${DLMDIR}.tar.gz

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)
GITVERSION:=$(shell cat .git/refs/heads/master)

DEBS=						\
libdlm3_${DLMVER}-${PKGREL}_${ARCH}.deb		\
libdlm-dev_${DLMVER}-${PKGREL}_${ARCH}.deb

all: ${DEBS}

.PHONY: dinstall
dinstall: deb
	dpkg -i ${DEBS}

.PHONY: deb
deb ${DEBS}: ${DLMSRC}
	rm -rf ${DLMDIR}
	tar xf ${DLMSRC}
	cp -a debian ${DLMDIR}
	cd ${DLMDIR}; dpkg-buildpackage -b -uc -us
	lintian ${DEBS}

download:
	rm -f ${DLMSRC}
	wget https://git.fedorahosted.org/cgit/dlm.git/snapshot/${DLMSRC}

.PHONY: upload
upload: ${DEBS}
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o rw 
	mkdir -p /pve/${RELEASE}/extra
	rm -f /pve/${RELEASE}/extra/libdlm3_*.deb
	rm -f /pve/${RELEASE}/extra/libdlm-dev_*.deb
	rm -f /pve/${RELEASE}/extra/Packages*
	cp ${DEBS} /pve/${RELEASE}/extra
	cd /pve/${RELEASE}/extra; dpkg-scanpackages . /dev/null > Packages; gzip -9c Packages > Packages.gz
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o ro

distclean: clean

.PHONY: clean
clean:
	rm -rf *~ *_${ARCH}.deb *.changes *.dsc ${DLMDIR}
	find . -name '*~' -exec rm {} ';'



